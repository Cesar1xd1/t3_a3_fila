import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

import javax.xml.bind.ValidationEvent;

interface correccion{
	Scanner entrada = new Scanner(System.in);
	public static int validacion() {
		int r = 0;
		boolean e = false;
		
		do {
			try {
				
				
				r = entrada.nextInt();
				
				
			} catch (java.util.InputMismatchException x) {
				System.out.println("Ups! el dato que intentas ingresar no es valido");
				entrada.nextLine();
				e=true;
			}
			if (r>0) {
				e=false;
			}else {
				System.out.println("Ingresa porfavor solo numeros mayores a 0");
				e=true;
			}
		}while(e);
		return r;
	}
}


class Impresion implements correccion{
	private int ide;
	private double size;
	private int hojasimp;
	
	public Impresion(int ide, double size, int hojasimp) {
		super();
		this.ide = ide;
		this.size =size;
		this.hojasimp = hojasimp;
	}
	
	public Impresion() {}
	public String toString() {
		return "Impresion [ide=" + ide + ", size=" + size + ", hojasimp=" + hojasimp + "]";
	}
	
	

	
}
interface RegistroImpresiones{
	public boolean verificarFilaLlena();
	
	
	
	public boolean verficarFilaVacia();
	public boolean agregarImpresion(Impresion i);
	public Impresion eliminarImpresion();
	
}
class ImplementacionFilaEstatica implements RegistroImpresiones{
	private Impresion fila[];
	private int t;
	private int pocicionF;
	public ImplementacionFilaEstatica(int t) {
		fila=new Impresion[t];
		pocicionF=-1;
		this.t=t;
	}
	public boolean verificarFilaLlena() {
		return pocicionF==t-1;
	}
	
	
	public boolean verficarFilaVacia() {
		return pocicionF==-1;
	}
	
	
	public boolean agregarImpresion(Impresion i) {
		if(!verificarFilaLlena()) {
			pocicionF++;
			fila[pocicionF]=i;
			return true;
		}
		return false;
	}
	
	
	public Impresion eliminarImpresion() {
		if(verficarFilaVacia()==false) {
			Impresion retorno=fila[pocicionF];
			fila[pocicionF]=null;
			pocicionF--;
			for(int i=0;i<pocicionF;i++) {
				fila[i]=fila[i+1];
			}
			return retorno;
			
		}else {
			System.out.println("Esta Vacio");
			return null;
		}
		
	}
	
	
	
	
}
class ImplementacionFilaDinamica implements RegistroImpresiones{
	Queue<Impresion> filaD =new LinkedList<Impresion>();
	private int t;
	private int pocicion;
	public ImplementacionFilaDinamica(int t) {
		super();
		this.t = t;
		pocicion = -1;
	}
	
	public boolean verificarFilaLlena() {
		return  pocicion == t-1;
	}
	
	public boolean verficarFilaVacia() {
		return filaD.isEmpty();
	}
	
	public boolean agregarImpresion(Impresion i) {
		if(!verificarFilaLlena()) {
			pocicion++;
			filaD.add(i);
			return true;
		}
		return false;
	}
	
	public Impresion eliminarImpresion() {
		if(verficarFilaVacia()==false) {
			pocicion = pocicion -1;
			return filaD.poll();
		}
		return null;
	}
	
}
public class PruebaFila {

	public static void main(String[] args) {
		Scanner entrada=new Scanner(System.in);
		System.out.println("=== La Fila tiene 5 pocisiones ===");
		
		ImplementacionFilaEstatica filaE=new ImplementacionFilaEstatica(5);
		ImplementacionFilaDinamica filaDi=new ImplementacionFilaDinamica(5);
		
		int op;
		int de=0;
		int ce=0;
		
		do {
			System.out.println("================ MENU =============== ");
			System.out.println("Digite 1 para agregar a la cola de impresion");
			System.out.println("Digite 2 para mandar a imprimir");
			System.out.println("Digite 3 para  ***SALIR***");
			
			
			op= correccion.validacion();
			
			
			
			switch (op) {
			
			case 1:
				System.out.println("A Cual impresora desea mandar a imprimir, a la 1 o a la 2?");
				int ops=correccion.validacion();
				if(ops == 2) {
					System.out.println("Ingrese el numero de hojas a imprimir");
					try {
						
					int numero= correccion.validacion();
					Impresion i=new Impresion(++de,0.0,numero);
					System.out.println(filaE.agregarImpresion(i)?"El documento fue añadido a la cola de impresion":"Fue imposible imprimir");
					
					
					}catch (NumberFormatException e) {
						System.out.println("Debes de ingresar un numero entero");
					}
					
				}else if(ops == 2){
					
					System.out.println("Numero de hojas que se van a imprimir");
					try {
					int numero=correccion.validacion();
					Impresion i2=new Impresion(++de,0.0,numero);
					System.out.println(filaDi.agregarImpresion(i2)?"El documento fue añadido a la cosa de impresion":"Fue imposible imprimir");
					}catch (NumberFormatException e) {
						System.out.println("Debes de ingresar un numero entero");
					}
	
				
				}System.out.println("[Enter para continuar]");
				entrada.nextLine();
				break;
			case 2:
				System.out.println("A Cual impresora desea mandar a imprimir, a la 1 o a la 2?");
				int ops2=correccion.validacion();
				if(ops2==1) {
					System.out.println("===== imprimio =====");
					System.out.println(filaE.eliminarImpresion());
					de=de-1;
				}else if(ops2==2) {
					System.out.println("===== Impresion =====");
					System.out.println(filaDi.eliminarImpresion());
					ce = ce-1;
				}
				break;
			case 3:
				System.out.println("Has salido del programa, Gracias por usarlo");
				break;
			}
		}while(op!=3);
	}

}
